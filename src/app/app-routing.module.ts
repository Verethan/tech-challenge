import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { EditUserAccountComponent } from './user-account/edit-account/edit-user-account.component';
import { ViewUserBlurbComponent } from './user-account/user-blurb/view-user-blurb.component';

const routes: Routes = [
  { path: 'techChallenge/user', component: LandingPageComponent, children: [
    { path: 'account', component: EditUserAccountComponent, outlet: 'user' },
    { path: 'blurb', component: ViewUserBlurbComponent, outlet: 'user' },
  ] },
  { path: '', redirectTo: '/techChallenge/user', pathMatch: 'full' },
  { path: 'techChallenge', redirectTo: '/techChallenge/user', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
