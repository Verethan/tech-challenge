import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { Subscription } from 'rxjs';
import { PaneSelectorService } from '../service/pane-selector.service';
import { trigger, transition, animate, style, state } from '@angular/animations';

@Component({
  selector: 'tc-landing-page-component',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
  animations: [
    trigger('slideInOutX', [
      transition(':enter', [
        style({transform: 'translateX(-60%)'}),
        animate('250ms ease-in', style({transform: 'translateX(0%)'}))
      ]),
      transition(':leave', animate('250ms ease-in', style({transform: 'translateX(-60%)'})))
    ]),
    trigger('slideInOutY', [
      transition(':enter', [
        style({transform: 'translateY(-60%)'}),
        animate('250ms ease-in', style({transform: 'translateY(0%)'}))
      ]),
      transition(':leave', animate('250ms ease-in', style({transform: 'translateY(-60%)'})))
    ])
  ]
})
export class LandingPageComponent implements OnInit, OnDestroy {

  paneRouteSelectorSubscription: Subscription;

  accountEditingActive = true;
  innerWidth = window.innerWidth;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }

  constructor(private paneRouteController: PaneSelectorService) { }

  ngOnInit() {
    this.paneRouteSelectorSubscription = this.paneRouteController.paneSelectorRoute
      .subscribe(route => {
        this.accountEditingActive = route === 'account';
        if (this.mobileDevice()) {
          if (route === 'account') {
            window.scrollTo(0, 892);
          }
        }
      });
  }

  mobileDevice(): boolean {
    return this.innerWidth < 1024;
  }

  ngOnDestroy() {
    if (this.paneRouteSelectorSubscription) {
      this.paneRouteSelectorSubscription.unsubscribe();
    }
  }

}
