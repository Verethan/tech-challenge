export interface UserAccount {
  userName: string;
  gender: Gender;
  dateOfBirth: string;
  emailAddress: string;
  mobileNumber: string;
  customerID: string;
  membership: Membership;
}

export enum Gender {
  male = 'male',
  female = 'female'
}

export enum Membership {
  classic = 'classic',
  silver = 'silver',
  gold = 'gold'
}
