import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IconComponent } from '../common/icons/icon.component';
import { IconsComponent } from '../common/icons/icons.component';

@NgModule({
  declarations: [
    IconComponent,
    IconsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  exports: [
    IconComponent,
    IconsComponent
  ]
})
export class CommonModule { }
