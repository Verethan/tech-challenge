import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'tc-icon',
  templateUrl: './icon.component.html'
})
export class IconComponent implements OnInit {

  @Input() name: string;
  @Input() heightPx: number;
  @Input() widthPx: number;

  constructor() { }

  ngOnInit() {
  }

  href(): string {
    return '#' + this.name;
  }


}
