import { TestBed } from '@angular/core/testing';

import { PaneSelectorService } from './pane-selector.service';

describe('PaneSelectorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaneSelectorService = TestBed.get(PaneSelectorService);
    expect(service).toBeTruthy();
  });
});
