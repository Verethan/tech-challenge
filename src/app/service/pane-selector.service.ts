import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PaneSelectorService {

  paneSelectorRoute = new EventEmitter<string>();

  constructor() { }

  emitSelectedRoute(route: string) {
    this.paneSelectorRoute.emit(route);
  }
}
