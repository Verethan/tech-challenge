import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserAccount, Gender, Membership } from 'src/app/common/model/user-account';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'tc-edit-user-account',
  templateUrl: './edit-user-account.component.html',
  styleUrls: ['./edit-user-account.component.scss'],
})
export class EditUserAccountComponent implements OnInit, OnDestroy {

  userAccount: UserAccount;
  userAccountForm: FormGroup;

  genders = Gender;
  memberships = Membership;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.userAccountForm = this.formBuilder.group({
      userName: ['', Validators.required],
      gender: [null, Validators.required],
      dateOfBirth: ['', Validators.required],
      emailAddress: ['', Validators.required],
      mobileNumber: ['', Validators.required],
      customerID: ['', Validators.required],
      membership: [null, Validators.required]
    });
  }

  ngOnDestroy() {
    setTimeout(null, 5000);
  }

  getFormControls() {
    return this.userAccountForm.controls;
  }

  selectGender(gend: string) {
    this.userAccountForm.patchValue({gender: gend});
  }

  isGenderSelected(gend: string): boolean {
    return this.userAccountForm.get('gender').value === gend;
  }

  selectMembership(plan: string) {
    this.userAccountForm.patchValue({membership: plan});
  }

  isMembershipSelected(plan: string): boolean {
    return this.userAccountForm.get('membership').value === plan;
  }

  cancel() {
    this.userAccountForm.reset();
  }

  submitUserAccountDetails() {
    console.log(this.userAccountForm.getRawValue());
  }

}
