import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewUserBlurbComponent } from './view-user-blurb.component';

describe('ViewUserBlurbComponent', () => {
  let component: ViewUserBlurbComponent;
  let fixture: ComponentFixture<ViewUserBlurbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUserBlurbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUserBlurbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
