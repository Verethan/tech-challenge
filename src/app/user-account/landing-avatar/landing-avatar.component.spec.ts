import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingAvatarComponent } from './landing-avatar.component';

describe('LandingAvatarComponent', () => {
  let component: LandingAvatarComponent;
  let fixture: ComponentFixture<LandingAvatarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandingAvatarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingAvatarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
