import { Component, OnInit } from '@angular/core';
import { PaneSelectorService } from 'src/app/service/pane-selector.service';

@Component({
  selector: 'tc-landing-avatar',
  templateUrl: './landing-avatar.component.html',
  styleUrls: ['./landing-avatar.component.scss']
})
export class LandingAvatarComponent implements OnInit {

  editingUserAccount = true;

  constructor(private paneRouteController: PaneSelectorService) { }

  ngOnInit() {
  }

  editUserAccount() {
    this.editingUserAccount = !this.editingUserAccount;
    this.paneRouteController.emitSelectedRoute(this.editingUserAccount ? 'account' : 'blurb');
  }

}
