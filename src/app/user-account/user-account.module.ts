import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LandingAvatarComponent } from './landing-avatar/landing-avatar.component';
import { ViewUserBlurbComponent } from './user-blurb/view-user-blurb.component';
import { EditUserAccountComponent } from './edit-account/edit-user-account.component';
import { CommonModule } from '../common/common.module';

@NgModule({
  declarations: [
    LandingAvatarComponent,
    ViewUserBlurbComponent,
    EditUserAccountComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ],
  providers: [],
  exports: [
    LandingAvatarComponent,
    ViewUserBlurbComponent,
    EditUserAccountComponent,
  ]
})
export class UserAccountModule { }
